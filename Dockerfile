# This file is a template, and might need editing before it works on your project.
FROM python:3.9

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.

RUN python --version

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# For Django
# EXPOSE 8000
# CMD ["python", "interact.py", "runserver", "0.0.0.0:8000"]

# For some other command
CMD ["python", "interact.py"]
